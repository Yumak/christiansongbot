import json
import time
from fuzzywuzzy import fuzz
import telebot
import psycopg2
import traceback
import requests

TOKEN = "1648983636:AAEXqhvNiXDZA_ZrQfWtsJn3dXrpglhLBzo"  # "1021990349:AAHq8IAkx8cZdf4_WJ3gI8A6iSzG4gSBxII"  #
SUPPORT_CHAT_ID = -1001343368276
ADMIN_CHAT_ID = 1093110311
ERROR_CHAT_ID = -1001490433883

bot = telebot.TeleBot(token=TOKEN, parse_mode="MARKDOWN")
conn = psycopg2.connect(dbname='christiansongsbot',
                        user='christiansongsbot',
                        password='2001',
                        host='46.17.104.151')
conn.autocommit = True
cursor = conn.cursor()

print("listen bot...")

data_songs = []
books = []

query_handler_press = {}

to_send_one_post_user_id = 0

button_number = {
    "1": "1️⃣",
    "2": "2️⃣",
    "3": "3️⃣",
    "4": "4️⃣",
    "5": "5️⃣",
    "6": "6️⃣",
    "7": "7️⃣",
    "8": "8️⃣",
    "9": "9️⃣",
    "0": "🔟"
}


def error(e: str, chat_id: int, text: str, args: str, inline: bool = False):
    # print(e)
    requests.get(f"https://api.telegram.org/bot{TOKEN}/sendmessage",
                 {
                     "chat_id": ERROR_CHAT_ID,
                     "text": f"chat-id: [{chat_id}](tg://user?id={chat_id})\n"
                             f"text: `{text}`\n"
                             f"time: `{str(time.ctime())}`\n"
                             f"args: `{args}`",
                     "parse_mode": "MARKDOWN"
                 })
    requests.get(f"https://api.telegram.org/bot{TOKEN}/sendmessage",
                 {
                     "chat_id": ERROR_CHAT_ID,
                     "text": f"```{e}```",
                     "parse_mode": "MARKDOWN"
                 })
    if not inline:
        requests.get(f"https://api.telegram.org/bot{TOKEN}/sendmessage",
                     {
                         "chat_id": chat_id,
                         "text": f"Приносим свои извенения, на сервере бота произошла ошибка, "
                                 f"пожалуйста перезагрузите бота командой /start и повторите запрос.",
                         "parse_mode": "MARKDOWN"
                     })


def sendMessage(chat_id: int, text: str, reply_markup: telebot.types.ReplyKeyboardMarkup = None,
                reply_to_message_id: int = None):
    try:
        bot.send_chat_action(chat_id=chat_id, action='typing')
        msg = bot.send_message(chat_id=chat_id,
                               text=text,
                               reply_markup=reply_markup,
                               reply_to_message_id=reply_to_message_id)
        return msg
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id, text, e.args)


def button_menu():
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row("⭐Избранное", "➕Добавить (предложить) песню")
    keyboard.row("Поучаствовать в проекте🤝")
    keyboard.row("📤Поддержка", "⁉Справка")
    return keyboard


def back(msg):
    try:
        bot.send_chat_action(chat_id=msg.chat.id, action='typing')
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {msg.chat.id}', ("start",))
        keyboard = button_menu()
        sendMessage(chat_id=msg.chat.id,
                    text=f"🔎Для поиска песни, напишите мне часть песни или номер ее в сборнике,"
                         f" и я вам предложу песню из моей базы.\n"
                         f"📝Если вы не найдете песню, зайдите в раздел `Добавить (предложить) песню`.",
                    reply_markup=keyboard)
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id=msg.chat.id, text=msg.text, args=e.args)


def song_text(title: str, book: str, number: int, tune: str, song: str) -> str:
    return f"*{title}*\n" \
           f"Из сборника _{book} №{number}_\n" \
           f"Тональность `{tune}`\n\n" \
           f"{song}\n\n\n" \
           f"@ChristianSongbot"


def help_text(user_name: str = None) -> str:
    if user_name is not None:
        hi = "*{user_name}*, добро пожаловать к нам👋.\n"
    else:
        hi = ""
    return f"{hi}" \
           f"🔎Для поиска песни, напишите мне часть песни или номер ее в сборнике," \
           f" и я вам предложу песню из моей базы.\n" \
           f"📝Если вы не найдете песню, зайдите в раздел `Добавить (предложить) песню`.\n" \
           f"У бота есть очень полезная функция, поиск можно осуществлять в любом чате telegram," \
           f"для этого напишите @ChristianSongbot в любом чате и текст из песни или ее номер в сборнике, " \
           f"бот предложит вам 10 вариантов, наиболее подходящих по вашему запросу. " \
           f"При таком поиске не доступна кнопка посмотреть ноты.\n" \
           f"Ноты можно посмотреть нажав на кнопку `Ноты` под сообщением с песней.\n" \
           f"Если у вас будут вопросы или предложения по боту, зайдите в раздел `Поддержка`\n" \
           f"В базе бота пока есть песни только 3 сборников, это `Песнь Возрождения`, `Тебе пою о мой Спаситель`" \
           f" и `Держись Христа`. Ноты есть только на сборник `Песнь Возрождения`."


def count_text(count: int, text0: str, text1: str, text2: str) -> str:
    if count == 0:
        result_text = f"{count} {text0}"
    elif 10 <= count % 100 <= 20:
        result_text = f"{count} {text0}"
    elif count % 10 == 1:
        result_text = f"{count} {text1}"
    elif 2 <= count % 10 <= 4:
        result_text = f"{count} {text2}"
    else:
        result_text = f"{count} {text0}"
    return result_text


def favorites(msg, page: int = 1, edit: bool = False):
    cursor.execute(
        f"SELECT favorites FROM users WHERE chat_id = {msg.chat.id}"
    )
    markup = telebot.types.InlineKeyboardMarkup()
    data_favorites = json.loads(cursor.fetchone()[0])
    length = len(data_favorites)
    len_text = count_text(length, "песен", "песня", "песни")
    if divmod(length, 10)[1] == 0:
        if divmod(length, 10)[0] == 0:
            pages = 1
        else:
            pages = divmod(length, 10)[0]
    else:
        pages = divmod(length, 10)[0] + 1
    text = f"В избранных `{len_text}`\n\n"
    i = 1
    for fav in data_favorites:
        if (page - 1) * 10 < i < (page * 10) + 1:
            cursor.execute(f"SELECT title, songbook_id, songbook_number FROM songs WHERE id = {int(fav)}")
            data_song = cursor.fetchone()
            cursor.execute(f"SELECT title FROM song_book WHERE id = {data_song[1]}")
            data_book = cursor.fetchone()
            text = text + f"{i - ((page - 1) * 10)} - {data_song[0]} (`{data_book[0]} №{data_song[2]}`)\n"
            if i % 10 == 1:
                markup.add(
                    telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                       callback_data=f'song {fav}')
                )
            elif 1 < i % 10 <= 5:
                markup.keyboard[0].append(
                    telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                       callback_data=f'song {fav}')
                )
            elif i % 10 == 6:
                markup.add(
                    telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                       callback_data=f'song {fav}')
                )
            else:
                markup.keyboard[1].append(
                    telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                       callback_data=f'song {fav}')
                )
        i = i + 1
    if pages > 1:
        if page == 1:
            markup.add(
                telebot.types.InlineKeyboardButton(text=f"⏩",
                                                   callback_data=f'favorites page {page + 1}'),
                telebot.types.InlineKeyboardButton(text=f"⏭",
                                                   callback_data=f'favorites page {pages}')
            )
        elif page == pages:
            markup.add(
                telebot.types.InlineKeyboardButton(text=f"⏮",
                                                   callback_data=f'favorites page {1}'),
                telebot.types.InlineKeyboardButton(text=f"⏪",
                                                   callback_data=f'favorites page {page - 1}')
            )
        else:
            markup.add(
                telebot.types.InlineKeyboardButton(text="⏮",
                                                   callback_data=f'favorites page {1}'))
            markup.keyboard[2].append(
                telebot.types.InlineKeyboardButton(text=f"⏪",
                                                   callback_data=f'favorites page {page - 1}'))
            markup.keyboard[2].append(
                telebot.types.InlineKeyboardButton(text=f"⏩",
                                                   callback_data=f'favorites page {page + 1}'))
            markup.keyboard[2].append(
                telebot.types.InlineKeyboardButton(text=f"⏭",
                                                   callback_data=f'favorites page {pages}')
            )
    text = text + f"\nСтраница `{page}` из `{pages}`"
    if edit:
        bot.edit_message_text(
            chat_id=msg.chat.id,
            message_id=msg.message_id,
            text=text,
            reply_markup=markup
        )
    else:
        sendMessage(
            chat_id=msg.chat.id,
            text=text,
            reply_markup=markup
        )


'''def search_book(msg, page: int = 1, edit: bool = False):
    cursor.execute(f"SELECT * FROM song_book")
    data_book = cursor.fetchall()
    markup = telebot.types.InlineKeyboardMarkup()
    length = len(data_book)
    if divmod(length, 10)[1] == 0:
        if divmod(length, 10)[0] == 0:
            pages = 1
        else:
            pages = divmod(length, 10)[0]
    else:
        pages = divmod(length, 10)[0] + 1
    text = f'В базе `{count_text(length, "сборников", "сборник", "сборника")}`\n' \
           f'Пожалуйста, выберите в каком сборнике вы желаете совершить поиск:\n\n'
    i = 1
    for book in sorted(data_book, key=lambda b: b[0]):
        if (page - 1) * 10 < i < (page * 10) + 1:
            text = text + f"{i - ((page - 1) * 10)} - {book[1]} (`{count_text(book[2], 'песен', 'песня', 'песни')}`)\n"
            if i % 10 == 1:
                markup.add(
                    telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                       callback_data=f'search_book {book[0]}')
                )
            elif 1 < i % 10 <= 5:
                markup.keyboard[0].append(
                    telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                       callback_data=f'search_book {book[0]}')
                )
            elif i % 10 == 6:
                markup.add(
                    telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                       callback_data=f'search_book {book[0]}')
                )
            else:
                markup.keyboard[1].append(
                    telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                       callback_data=f'search_book {book[0]}')
                )
        i = i + 1
    if pages > 1:
        if page == 1:
            markup.add(
                telebot.types.InlineKeyboardButton(text=f"⏩",
                                                   callback_data=f'search_book page {page + 1}'),
                telebot.types.InlineKeyboardButton(text=f"⏭",
                                                   callback_data=f'search_book page {pages}')
            )
        elif page == pages:
            markup.add(
                telebot.types.InlineKeyboardButton(text=f"⏮",
                                                   callback_data=f'search_book page {1}'),
                telebot.types.InlineKeyboardButton(text=f"⏪",
                                                   callback_data=f'search_book page {page - 1}')
            )
        else:
            markup.add(
                telebot.types.InlineKeyboardButton(text="⏮",
                                                   callback_data=f'search_book page {1}'))
            markup.keyboard[2].append(
                telebot.types.InlineKeyboardButton(text=f"⏪",
                                                   callback_data=f'search_book page {page - 1}'))
            markup.keyboard[2].append(
                telebot.types.InlineKeyboardButton(text=f"⏩",
                                                   callback_data=f'search_book page {page + 1}'))
            markup.keyboard[2].append(
                telebot.types.InlineKeyboardButton(text=f"⏭",
                                                   callback_data=f'search_book page {pages}')
            )
    text = text + f"\nСтраница `{page}` из `{pages}`"
    if edit:
        bot.edit_message_text(
            chat_id=msg.chat.id,
            message_id=msg.message_id,
            text=text,
            reply_markup=markup
        )
    else:
        sendMessage(
            chat_id=msg.chat.id,
            text=text,
            reply_markup=markup
        )


def search_subject(msg, page: int = 1, edit: bool = False):
    cursor.execute(f"SELECT * FROM subject")
    data_subject = cursor.fetchall()
    markup = telebot.types.InlineKeyboardMarkup()
    length = len(data_subject)
    if divmod(length, 10)[1] == 0:
        if divmod(length, 10)[0] == 0:
            pages = 1
        else:
            pages = divmod(length, 10)[0]
    else:
        pages = divmod(length, 10)[0] + 1
    text = f'В базе `{count_text(length, "категорий", "категория", "категории")}`\n' \
           f'Пожалуйста, выберите в какой категории вы желаете совершить поиск:\n\n'
    i = 1
    for subject in sorted(data_subject, key=lambda b: b[0]):
        if (page - 1) * 10 < i < (page * 10) + 1:
            text = text + f"{i - ((page - 1) * 10)} - {subject[1]}\n"
            if i % 10 == 1:
                markup.add(
                    telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                       callback_data=f'search_subject {subject[0]}')
                )
            elif 1 < i % 10 <= 5:
                markup.keyboard[0].append(
                    telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                       callback_data=f'search_subject {subject[0]}')
                )
            elif i % 10 == 6:
                markup.add(
                    telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                       callback_data=f'search_subject {subject[0]}')
                )
            else:
                markup.keyboard[1].append(
                    telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                       callback_data=f'search_subject {subject[0]}')
                )
        i = i + 1
    if pages > 1:
        if page == 1:
            markup.add(
                telebot.types.InlineKeyboardButton(text=f"⏩",
                                                   callback_data=f'search_subject page {page + 1}'),
                telebot.types.InlineKeyboardButton(text=f"⏭",
                                                   callback_data=f'search_subject page {pages}')
            )
        elif page == pages:
            markup.add(
                telebot.types.InlineKeyboardButton(text=f"⏮",
                                                   callback_data=f'search_subject page {1}'),
                telebot.types.InlineKeyboardButton(text=f"⏪",
                                                   callback_data=f'search_subject page {page - 1}')
            )
        else:
            markup.add(
                telebot.types.InlineKeyboardButton(text="⏮",
                                                   callback_data=f'search_subject page {1}'))
            markup.keyboard[2].append(
                telebot.types.InlineKeyboardButton(text=f"⏪",
                                                   callback_data=f'search_subject page {page - 1}'))
            markup.keyboard[2].append(
                telebot.types.InlineKeyboardButton(text=f"⏩",
                                                   callback_data=f'search_subject page {page + 1}'))
            markup.keyboard[2].append(
                telebot.types.InlineKeyboardButton(text=f"⏭",
                                                   callback_data=f'search_subject page {pages}')
            )
    text = text + f"\nСтраница `{page}` из `{pages}`"
    if edit:
        bot.edit_message_text(
            chat_id=msg.chat.id,
            message_id=msg.message_id,
            text=text,
            reply_markup=markup
        )
    else:
        sendMessage(
            chat_id=msg.chat.id,
            text=text,
            reply_markup=markup
        )'''


def all_post(msg):
    bot.send_chat_action(chat_id=msg.chat.id, action='typing')
    if msg.chat.id != ADMIN_CHAT_ID:
        return
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {msg.chat.id}', ("all_post",))
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row("🔙Назад")
    sendMessage(chat_id=ADMIN_CHAT_ID,
                text="Введите текст, который отправить всем пользователям.",
                reply_markup=keyboard)


def one_user_post(msg):
    bot.send_chat_action(chat_id=msg.chat.id, action='typing')
    if msg.chat.id != ADMIN_CHAT_ID:
        return
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {msg.chat.id}', ("one_user_post",))
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row("🔙Назад")
    sendMessage(chat_id=msg.chat.id,
                text="Пришлите мне chat-id пользователя, кому отправить пост.",
                reply_markup=keyboard)


def one_user_post_text(msg):
    if msg.chat.id != ADMIN_CHAT_ID:
        return
    bot.send_chat_action(chat_id=msg.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {msg.chat.id}', ("one_user_post_text",))
    cursor.execute(f'SELECT chat_id, first_name, last_name FROM users WHERE chat_id = {to_send_one_post_user_id}')
    data = cursor.fetchone()
    if not data:
        one_user_post(msg)
    else:
        sendMessage(chat_id=ADMIN_CHAT_ID,
                    text=f'Введите текст, который отправить пользователю:\n'
                         f'{data[1]} {data[2]} | [{data[0]}](tg://user?id={data[0]})')


def statistic(msg):  # Доделать статистику по песням и книгам
    bot.send_chat_action(chat_id=msg.chat.id, action='typing')
    if msg.chat.id != ADMIN_CHAT_ID:
        return
    cursor.execute(f'SELECT chat_id, first_name, last_name, username, favorites FROM users')
    users = cursor.fetchall()
    bot.send_chat_action(chat_id=msg.chat.id, action='upload_document')
    with open('files/users.txt', 'w') as f:
        for user in users:
            f.write(
                f"{user[0]} | {user[3]} | "
                f"favorites: {count_text(len(json.loads(user[4])), 'песен', 'песня', 'песни')}\n"
            )
    with open('files/users.txt', 'rb') as f_user:
        bot.send_media_group(chat_id=ADMIN_CHAT_ID,
                             media=[
                                 telebot.types.InputMediaDocument(media=f_user,
                                                                  caption='Список пользователей☝\n'
                                                                          f'Всего {len(users)} пользователей')
                             ])
    admin_panel(msg)


def send_song(msg):
    cursor.execute(
        f"SELECT songbook_id, title, songbook_number, tune, song FROM songs "
        f"WHERE id = {int(msg.text[5:])}")
    data_song = cursor.fetchone()
    cursor.execute(f"SELECT title FROM song_book WHERE id = {data_song[0]}")
    data_book = cursor.fetchone()
    cursor.execute(
        f"SELECT favorites FROM users WHERE chat_id = {msg.chat.id}")
    data_favorites = json.loads(cursor.fetchone()[0])
    markup = telebot.types.InlineKeyboardMarkup()
    markup.add(
        telebot.types.InlineKeyboardButton(text='ℹО песне',
                                           callback_data=f'info_song {int(msg.text[5:])}'),
        telebot.types.InlineKeyboardButton(text='🎵Ноты',
                                           callback_data=f'noty {int(msg.text[5:])}')
    )
    if int(msg.text[5:]) in data_favorites:
        markup.add(
            telebot.types.InlineKeyboardButton(text='🌟Удалить из избранного',
                                               callback_data=f'favorites delete {int(msg.text[5:])}')
        )
    else:
        markup.add(
            telebot.types.InlineKeyboardButton(text='⭐Добавить в избранное',
                                               callback_data=f'favorites add {int(msg.text[5:])}')
        )
    markup.add(
        telebot.types.InlineKeyboardButton(text="🔎Искать в чате",
                                           switch_inline_query=""),
        telebot.types.InlineKeyboardButton(text='❌Закрыть',
                                           callback_data='delete'))
    sendMessage(chat_id=msg.chat.id,
                text=song_text(
                    title=data_song[1],
                    book=data_book[0],
                    number=data_song[2],
                    tune=data_song[3],
                    song=data_song[4]
                ),
                reply_markup=markup)


@bot.message_handler(commands=['start'])
def start(msg):
    try:
        bot.send_chat_action(chat_id=msg.chat.id, action='typing')
        if msg.chat.id == SUPPORT_CHAT_ID or msg.chat.type != 'private':
            return
        elif msg.text == "/start" or msg.text == "/start start":
            cursor.execute(f'SELECT * FROM users WHERE chat_id = {msg.chat.id}')
            data = cursor.fetchone()
            if not data:
                cursor.execute(
                    f'INSERT INTO users (chat_id, first_name, last_name, username, status) '
                    f'VALUES (%s, %s, %s, %s, %s)',
                    (msg.chat.id, str(msg.from_user.first_name), str(msg.from_user.last_name),
                     str(msg.from_user.username), "start"))
                text = help_text(msg.from_user.first_name)
            else:
                cursor.execute(f'UPDATE users SET first_name = %s, last_name = %s, username = %s,'
                               f'status = %s WHERE chat_id = {msg.chat.id}',
                               (str(msg.from_user.first_name), str(msg.from_user.last_name),
                                str(msg.from_user.username), "start"))
                text = "🔎Для поиска песни, напишите мне часть песни или номер ее в сборнике," \
                       f" и я вам предложу песню из моей базы.\n" \
                       f"📝Если вы не найдете песню, зайдите в раздел `Добавить (предложить) песню`."
            keyboard = button_menu()
            sendMessage(chat_id=msg.chat.id,
                        text=text,
                        reply_markup=keyboard)
        else:
            search_content(msg, text=msg.text[6:])
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id=msg.chat.id, text=msg.text, args=e.args)


@bot.message_handler(commands=['help'])
def help(msg):
    try:
        if msg.chat.id == SUPPORT_CHAT_ID or msg.chat.type != 'private':
            return
        else:
            bot.send_chat_action(chat_id=msg.chat.id, action='typing')
            cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {msg.chat.id}', ("start",))
            keyboard = button_menu()
            sendMessage(chat_id=msg.chat.id,
                        text=help_text(),
                        reply_markup=keyboard)
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id=msg.chat.id, text=msg.text, args=e.args)


@bot.message_handler(commands=['support'])
def support(msg):
    try:
        bot.send_chat_action(chat_id=msg.chat.id, action='typing')
        if msg.chat.id == SUPPORT_CHAT_ID or msg.chat.type != 'private':
            return
        else:
            cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {msg.chat.id}', ("support",))
            sendMessage(chat_id=msg.chat.id,
                        text="Пишите любое текстовое сообщение, и оно будет перенаправлено администраторам.",
                        reply_markup=telebot.types.ReplyKeyboardMarkup(True, True).row("🔙Назад"))
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id=msg.chat.id, text=msg.text, args=e.args)


@bot.message_handler(commands=['new_song'])
def new_song(msg):
    try:
        if msg.chat.id == SUPPORT_CHAT_ID or msg.chat.type != 'private':
            pass
        else:
            bot.send_chat_action(chat_id=msg.chat.id, action='typing')
            cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {msg.chat.id}', ("new_song",))
            sendMessage(chat_id=msg.chat.id,
                        text="Напишите текст песни или название песни, мы рассмотрим ее и добавим.\n"
                             "После добавления(отказа) мы пришлем вам уведомление.\n"
                             "Также в этом разделе можно предложить исправление какой либо песни.\n"
                             "Просьба указывать из какого сборника предложеная вами песня.",
                        reply_markup=telebot.types.ReplyKeyboardMarkup(
                            True, True).row("🔙Назад"))
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id=msg.chat.id, text=msg.text, args=e.args)


"""@bot.message_handler(commands=['search'])
def search(msg):
    try:
        if msg.chat.id == SUPPORT_CHAT_ID or msg.chat.type != 'private':
            pass
        else:
            if msg.text == '/search' or msg.text == "🔍Поиск по тексту(номеру)":
                cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {msg.chat.id}', ("search_content",))
                sendMessage(chat_id=msg.chat.id,
                            text="Пришлите мне текст песни (не менее 3 символов) или ее номер в сборнике,"
                                 " я попробую отыскать его среди песен в базе, и выдам до 10 "
                                 "результатов наиболее подходящих вашему запросу.",
                            reply_markup=telebot.types.ReplyKeyboardMarkup(True, True).row("🔙Назад"))
            else:
                search_content(msg, msg.text[8:])
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id=msg.chat.id, text=msg.text, args=e.args)
"""


@bot.message_handler(commands=['adminpanel'])
def admin_panel(msg):
    if msg.chat.id != ADMIN_CHAT_ID:
        return
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {msg.chat.id}', ("admin_panel",))
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row("Опубликовать пост всем", "Отправить пост одному")
    keyboard.row("Получить статистику", "🔙Назад")
    sendMessage(chat_id=ADMIN_CHAT_ID,
                text="Вы зашли в панель администратора, выберите из меню что желаете сделать.",
                reply_markup=keyboard)


@bot.message_handler(content_types=['text'])
def message(msg):
    global to_send_one_post_user_id
    try:
        if msg.text[:5] == "/song":
            send_song(msg)
        elif msg.chat.id == SUPPORT_CHAT_ID:
            if msg.reply_to_message == None:
                return
            cursor.execute(f"SELECT * FROM messages WHERE message_id = {msg.reply_to_message.message_id};")
            data = cursor.fetchone()
            bot.send_chat_action(chat_id=data[0], action='typing')
            msg_1 = bot.send_message(chat_id=data[0],
                                     reply_to_message_id=data[2],
                                     text=f"{msg.text}")
            cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                           f"VALUES (0, {msg_1.message_id}, {msg.message_id});")
        else:
            cursor.execute(f'SELECT * FROM users WHERE chat_id = {msg.chat.id}')
            data = cursor.fetchone()
            if data[4] == 'start':
                if msg.text == "⁉Справка":
                    help(msg)
                elif msg.text == "📤Поддержка":
                    support(msg)
                elif msg.text == "➕Добавить (предложить) песню":
                    new_song(msg)
                elif msg.text == "Поучаствовать в проекте🤝":
                    sendMessage(chat_id=msg.chat.id,
                                text=f"Если Вы желаете поддержать наш проект, "
                                     f"можете [перевести](https://sobe.ru/na/ChristianSongbot) нам любую сумму, "
                                     f"мы будем Вам очень Благодарны.\n"
                                     f"P.S. Помочь можно не только материально, если вы обладаете умением пользоваться "
                                     f"MS Word и имеете компьютер(ноутбук или PC), пишите нам. Нужно набирать"
                                     f"(искать) текст "
                                     f"в определенном формате для удобного и быстрого добавления его в базу бота.",
                                reply_markup=button_menu())
                elif msg.text == "⭐Избранное":
                    favorites(msg)
                elif msg.entities is None:
                    search_content(msg, msg.text)
            elif data[4] == 'support':
                if msg.text == "🔙Назад":
                    back(msg)
                else:
                    bot.send_chat_action(chat_id=SUPPORT_CHAT_ID, action='typing')
                    if msg.reply_to_message == None:
                        msg_1 = bot.send_message(chat_id=SUPPORT_CHAT_ID,
                                                 text=f"#support {msg.from_user.first_name} {msg.from_user.last_name}\n"
                                                      f"chat-id = [{msg.chat.id}](tg://user?id={msg.chat.id})"
                                                      f"\n```{msg.text}```", )
                    else:
                        cursor.execute(f'SELECT * FROM messages WHERE from_id = 0 AND '
                                       f'message_id = {msg.reply_to_message.message_id};')
                        data = cursor.fetchone()
                        msg_1 = bot.send_message(chat_id=SUPPORT_CHAT_ID,
                                                 text=f"#support {msg.from_user.first_name} {msg.from_user.last_name}\n"
                                                      f"chat-id = [{msg.chat.id}](tg://user?id={msg.chat.id})"
                                                      f"\n```{msg.text}```",
                                                 reply_to_message_id=data[2])
                    cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                                   f"VALUES (%s, %s, %s)", (msg.chat.id, msg_1.message_id, msg.message_id,))
            elif data[4] == "new_song":
                if msg.text == "🔙Назад":
                    back(msg)
                else:
                    bot.send_chat_action(chat_id=SUPPORT_CHAT_ID, action='typing')
                    markup = telebot.types.InlineKeyboardMarkup()
                    add = telebot.types.InlineKeyboardButton(text='Добавлена', callback_data='add')
                    cancel = telebot.types.InlineKeyboardButton(text='Отклонить', callback_data='cancel')
                    markup.add(add, cancel)
                    msg_1 = bot.send_message(chat_id=SUPPORT_CHAT_ID,
                                             text=f"#newSong {msg.from_user.first_name} {msg.from_user.last_name}"
                                                  f"chat-id = [{msg.chat.id}](tg://user?id={msg.chat.id})"
                                                  f"\n{msg.text}",
                                             reply_markup=markup)
                    cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                                   f"VALUES (%s, %s, %s)", (msg.chat.id, msg_1.message_id, msg.message_id,))
            elif data[4] == 'admin_panel':
                if msg.text == "🔙Назад":
                    back(msg)
                elif msg.text == "Опубликовать пост всем":
                    all_post(msg)
                elif msg.text == "Отправить пост одному":
                    one_user_post(msg)
                elif msg.text == "Получить статистику":
                    statistic(msg)
            elif data[4] == 'all_post':
                if msg.text == "🔙Назад":
                    admin_panel(msg)
                else:  # Нужно будет распределить на отдельный поток, до 10 сообщений в секунду или меньше.
                    cursor.execute(f'SELECT chat_id FROM users')
                    users = cursor.fetchall()
                    for user in users:
                        sendMessage(chat_id=user[0],
                                    text=f'*От администраторов:*\n{msg.text}')
                    admin_panel(msg)
            elif data[4] == 'one_user_post':
                if msg.text == "🔙Назад":
                    admin_panel(msg)
                else:
                    if msg.text.isdigit():
                        to_send_one_post_user_id = int(msg.text)
                    if to_send_one_post_user_id > 0:
                        one_user_post_text(msg)
                    else:
                        one_user_post(msg)
            elif data[4] == 'one_user_post_text':
                if msg.text == "🔙Назад":
                    one_user_post(msg)
                else:
                    sendMessage(chat_id=to_send_one_post_user_id,
                                text=f'*От администраторов:*\n{msg.text}')
                    to_send_one_post_user_id = 0
                    admin_panel(msg)
            '''elif data[4] == "search_book":
                if msg.text == "🔙Назад":
                    cursor.execute(f"UPDATE users SET search_book = null WHERE chat_id = {msg.chat.id}")
                    back(msg)
                else:
                    search_content(msg, msg.text, book=data[6])
            elif data[4] == "search_subject":
                if msg.text == "🔙Назад":
                    cursor.execute(f"UPDATE users SET search_subject = null WHERE chat_id = {msg.chat.id}")
                    back(msg)
                else:
                    search_content(msg, msg.text, subject=data[7])'''
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id=msg.chat.id, text=msg.text, args=e.args)


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    global query_handler_press
    try:
        try:
            if query_handler_press[call.from_user.id]:
                bot.answer_callback_query(call.id, text="Ну и куда ты спешишь?")
                return
        except KeyError as er:
            query_handler_press[call.from_user.id] = call.data
        if call.data == 'add':
            cursor.execute(f"SELECT * FROM messages WHERE message_id = {call.message.message_id};")
            data = cursor.fetchone()
            sendMessage(chat_id=data[0],
                        reply_to_message_id=data[2],
                        text=f"Предложеная песня добавлена")
            bot.edit_message_text(text=f"{call.message.text}\n✅",
                                  chat_id=call.message.chat.id,
                                  message_id=call.message.message_id,
                                  reply_markup=None)
            bot.answer_callback_query(call.id)
        elif call.data == 'cancel':
            cursor.execute(f"SELECT * FROM messages WHERE message_id = {call.message.message_id};")
            data = cursor.fetchone()
            sendMessage(chat_id=data[0],
                        reply_to_message_id=data[2],
                        text=f"Предложеная песня отклонена в добавлении.")
            bot.edit_message_text(text=f"{call.message.text}\n❌",
                                  chat_id=call.message.chat.id,
                                  message_id=call.message.message_id,
                                  reply_markup=None)
            bot.answer_callback_query(call.id)
        elif call.data == "info_search":
            sendMessage(chat_id=call.message.chat.id,
                        text=f"Пришлите мне текст(не менее 3 символов),"
                             " я попробую отыскать его среди песен в базе, и выдам до 10 "
                             "результатов наиболее подходящих вашему запросу.")
            bot.answer_callback_query(call.id)
        elif call.data == "delete":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            bot.answer_callback_query(call.id, text="✅Сообщение удалено✅")
        elif call.data[0:4] == "song":
            cursor.execute(
                f"SELECT songbook_id, title, songbook_number, tune, song FROM songs "
                f"WHERE id = {int(call.data[5:])}")
            data_song = cursor.fetchone()
            cursor.execute(f"SELECT title FROM song_book WHERE id = {data_song[0]}")
            data_book = cursor.fetchone()
            cursor.execute(
                f"SELECT favorites FROM users WHERE chat_id = {call.message.chat.id}")
            data_favorites = json.loads(cursor.fetchone()[0])
            markup = telebot.types.InlineKeyboardMarkup()
            markup.add(
                telebot.types.InlineKeyboardButton(text='ℹО песне',
                                                   callback_data=f'info_song {int(call.data[5:])}'),
                telebot.types.InlineKeyboardButton(text='🎵Ноты',
                                                   callback_data=f'noty {int(call.data[5:])}')
            )
            if int(call.data[5:]) in data_favorites:
                markup.add(
                    telebot.types.InlineKeyboardButton(text='🌟Удалить из избранного',
                                                       callback_data=f'favorites delete {int(call.data[5:])}')
                )
            else:
                markup.add(
                    telebot.types.InlineKeyboardButton(text='⭐Добавить в избранное',
                                                       callback_data=f'favorites add {int(call.data[5:])}')
                )
            markup.add(
                telebot.types.InlineKeyboardButton(text="🔎Искать в чате",
                                                   switch_inline_query=""),
                telebot.types.InlineKeyboardButton(text='❌Закрыть',
                                                   callback_data='delete'))
            sendMessage(chat_id=call.message.chat.id,
                        text=song_text(
                            title=data_song[1],
                            book=data_book[0],
                            number=data_song[2],
                            tune=data_song[3],
                            song=data_song[4]
                        ),
                        reply_markup=markup)
            bot.answer_callback_query(call.id, text="")
        elif call.data[0:9] == "info_song":
            cursor.execute(
                f"SELECT songbook_id, title, songbook_number, created_at, updated_at, tune, subject, noty "
                f"FROM songs WHERE id = {int(call.data[10:])}")
            data_song = cursor.fetchone()
            cursor.execute(f"SELECT title FROM song_book WHERE id = {data_song[0]}")
            data_book = cursor.fetchone()
            cursor.execute(f"SELECT name FROM subject WHERE id = {data_song[0]}")
            data_subject = cursor.fetchone()
            if data_song[4] == 0:
                date = "➖"
            else:
                date = time.strftime('%d.%m.%Y', time.gmtime(data_song[4]))
            if data_song[7] == "":
                noty = "❌"
            else:
                noty = "✅"
            bot.answer_callback_query(call.id,
                                      text=f"Название: {data_song[1]}\n"
                                           f"Сборник: {data_book[0]} №{data_song[2]}\n"
                                           f"Категория: {data_subject[0]}\n"
                                           f"Тональность: {data_song[5]}\n"
                                           f"Ноты: {noty}\n"
                                           f"Добавлена: {time.strftime('%d.%m.%Y', time.gmtime(data_song[3]))}\n"
                                           f"Изменения: {date}",
                                      show_alert=True, )
        elif call.data[0:4] == "noty":
            cursor.execute(
                f"SELECT noty FROM songs WHERE id = {int(call.data[5:])}")
            data_noty = cursor.fetchone()
            if data_noty[0] == "":
                noty_text = "⭕Нот пока еще нет в базе, но мы работаем над этим..."
            else:
                noty_json = json.loads(data_noty[0])
                noty_text = ""
                for noty in noty_json["noty"]:
                    noty_text = noty_text + f"[{noty['name']}]({noty['href']})\n"
            markup = telebot.types.InlineKeyboardMarkup()
            markup.add(
                telebot.types.InlineKeyboardButton(text='❌Закрыть',
                                                   callback_data='delete')
            )
            sendMessage(chat_id=call.message.chat.id,
                        text=noty_text,
                        reply_markup=markup)
            bot.answer_callback_query(call.id,
                                      text="Отправил😊")
        elif call.data[0:9] == "favorites":
            if call.data[10:16] == "delete":
                cursor.execute(
                    f"SELECT favorites FROM users WHERE chat_id = {call.message.chat.id}")
                data_favorites = json.loads(cursor.fetchone()[0])
                data_favorites.remove(int(call.data[17:]))
                cursor.execute(
                    f"UPDATE users SET favorites = %s WHERE chat_id = {call.message.chat.id}",
                    (json.dumps(data_favorites),)
                )
                markup = telebot.types.InlineKeyboardMarkup()
                markup.add(
                    telebot.types.InlineKeyboardButton(text='ℹО песне',
                                                       callback_data=f'info_song {int(call.data[17:])}'),
                    telebot.types.InlineKeyboardButton(text='🎵Ноты',
                                                       callback_data=f'noty {int(call.data[17:])}')
                )
                markup.add(
                    telebot.types.InlineKeyboardButton(text='⭐Добавить в избранное',
                                                       callback_data=f'favorites add {int(call.data[17:])}')
                )
                markup.add(
                    telebot.types.InlineKeyboardButton(text="🔎Искать в чате",
                                                       switch_inline_query=""),
                    telebot.types.InlineKeyboardButton(text='❌Закрыть',
                                                       callback_data='delete'))
                try:
                    bot.edit_message_reply_markup(chat_id=call.message.chat.id,
                                                  message_id=call.message.message_id,
                                                  reply_markup=markup
                                                  )
                    bot.answer_callback_query(call.id, text="✅Сообщение удалено из избранных")
                except Exception as e:
                    pass
            elif call.data[10:13] == "add":
                cursor.execute(
                    f"SELECT favorites FROM users WHERE chat_id = {call.message.chat.id}")
                data_favorites = json.loads(cursor.fetchone()[0])
                data_favorites.append(int(call.data[14:]))
                cursor.execute(
                    f"UPDATE users SET favorites = %s WHERE chat_id = {call.message.chat.id}",
                    (json.dumps(data_favorites),)
                )
                markup = telebot.types.InlineKeyboardMarkup()
                markup.add(
                    telebot.types.InlineKeyboardButton(text='ℹО песне',
                                                       callback_data=f'info_song {int(call.data[14:])}'),
                    telebot.types.InlineKeyboardButton(text='🎵Ноты',
                                                       callback_data=f'noty {int(call.data[14:])}')
                )
                markup.add(
                    telebot.types.InlineKeyboardButton(text='🌟Удалить из избранного',
                                                       callback_data=f'favorites delete {int(call.data[14:])}')
                )
                markup.add(
                    telebot.types.InlineKeyboardButton(text="🔎Искать в чате",
                                                       switch_inline_query=""),
                    telebot.types.InlineKeyboardButton(text='❌Закрыть',
                                                       callback_data='delete'))
                try:
                    bot.edit_message_reply_markup(chat_id=call.message.chat.id,
                                                  message_id=call.message.message_id,
                                                  reply_markup=markup
                                                  )
                    bot.answer_callback_query(call.id, text="✅Сообщение добавлено в избранные")
                except Exception as e:
                    pass
            elif call.data[10:14] == "page":
                favorites(msg=call.message, page=int(call.data[15:]), edit=True)
                bot.answer_callback_query(call.id)
        '''elif call.data[0:11] == "search_book":
            if call.data[12:].isdigit():
                cursor.execute(f'UPDATE users SET status = %s, search_book = %s '
                               f'WHERE chat_id = {call.message.chat.id}',
                               ("search_book", int(call.data[12:])))
                cursor.execute(f"SELECT title, count FROM song_book WHERE id = {int(call.data[12:])}")
                data_book = cursor.fetchone()
                bot.delete_message(chat_id=call.message.chat.id,
                                   message_id=call.message.message_id)
                sendMessage(
                    chat_id=call.message.chat.id,
                    text=f"Пришлите мне текст песни (не менее 3 символов) или ее номер в сборнике,"
                         f" я попробую отыскать его среди песен в базе, и выдам до 10 "
                         f"результатов наиболее подходящих вашему запросу.\n"
                         f"Поиск осуществляется в сборнике"
                         f" `{data_book[0]}` `({count_text(data_book[1], 'песен', 'песня', 'песни')})`",
                    reply_markup=telebot.types.ReplyKeyboardMarkup(True, True).row("🔙Назад")
                )
                bot.answer_callback_query(call.id)
            elif call.data[12:16] == "page":
                search_book(msg=call.message, page=int(call.data[17:]), edit=True)
                bot.answer_callback_query(call.id)
        elif call.data[0:14] == "search_subject":
            if call.data[15:].isdigit():
                cursor.execute(f'UPDATE users SET status = %s, search_subject = %s '
                               f'WHERE chat_id = {call.message.chat.id}',
                               ("search_subject", int(call.data[15:])))
                cursor.execute(f"SELECT name FROM subject WHERE id = {int(call.data[15:])}")
                data_subject = cursor.fetchone()
                bot.delete_message(chat_id=call.message.chat.id,
                                   message_id=call.message.message_id)
                sendMessage(
                    chat_id=call.message.chat.id,
                    text=f"Пришлите мне текст песни (не менее 3 символов) или ее номер в сборнике,"
                         f" я попробую отыскать его среди песен в базе, и выдам до 10 "
                         f"результатов наиболее подходящих вашему запросу.\n"
                         f"Поиск осуществляется в категории `{data_subject[0]}`",
                    reply_markup=telebot.types.ReplyKeyboardMarkup(True, True).row("🔙Назад")
                )
                bot.answer_callback_query(call.id)
            elif call.data[15:19] == "page":
                search_subject(msg=call.message, page=int(call.data[20:]), edit=True)
                bot.answer_callback_query(call.id)'''
        query_handler_press.pop(call.from_user.id, None)
    except Exception as e:
        query_handler_press.pop(call.from_user.id, None)
        if not call.inline_message_id:
            chat_id = call.message.chat.id
        else:
            chat_id = call.from_user.id
        error(traceback.format_exc(limit=1010), chat_id=chat_id, text=call.data, args=e.args)


def search_content(msg, text: str, book: int = None, subject: int = None):
    try:
        bot.send_chat_action(chat_id=msg.chat.id, action='typing')
        markup = telebot.types.InlineKeyboardMarkup()
        if text.isdigit():  # Поиск по номеру
            msg1 = sendMessage(chat_id=msg.chat.id,
                               text="*Ищу...*")
            if not book:
                if not subject:
                    cursor.execute("SELECT id, songbook_number FROM songs")
                    data = cursor.fetchall()
                else:
                    cursor.execute(f"SELECT id, songbook_number FROM songs WHERE subject = {subject}")
                    data = cursor.fetchall()
            else:
                cursor.execute(f"SELECT id, songbook_number FROM songs WHERE songbook_id = '{book}'")
                data = cursor.fetchall()
            result = []
            result_text = ""
            result_text1 = ""
            i = 1
            for song in data:
                if song[1] == int(text):  # fuzz.WRatio(text, song[2]) >= 60:
                    result.append({"id": song[0], "number": song[1]})
            for res in sorted(result, key=lambda r: r["number"]):
                if i > 10:
                    break
                cursor.execute(f"SELECT title, songbook_id, songbook_number, id FROM songs WHERE id = {res['id']}")
                data_song = cursor.fetchone()
                cursor.execute(f"SELECT title FROM song_book WHERE id = {data_song[1]}")
                data_book = cursor.fetchone()
                #  result_text = result_text + f"{i} - {data_song[0]} (`{data_book[0]} №{data_song[2]}`)\n"
                result_text1 = result_text1 + f"Название: `{data_song[0]}`\n" \
                                              f"Сборник: `{data_book[0]} №{data_song[2]}`\n" \
                                              f"Посмотреть: /song{data_song[3]}\n\n"
                """if i % 10 == 1:
                    markup.add(
                        telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                           callback_data=f'song {res["id"]}')
                    )
                elif 1 < i % 10 <= 5:
                    markup.keyboard[0].append(
                        telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                           callback_data=f'song {res["id"]}')
                    )
                elif i % 10 == 6:
                    markup.add(
                        telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                           callback_data=f'song {res["id"]}')
                    )
                else:
                    markup.keyboard[1].append(
                        telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                           callback_data=f'song {res["id"]}')
                    )"""
                i = i + 1
        elif len(text) < 3:
            markup.add(
                telebot.types.InlineKeyboardButton(text='❔Как искать?',
                                                   callback_data='info_search'))
            markup.add(
                telebot.types.InlineKeyboardButton(text="🔎Искать в чате",
                                                   switch_inline_query="")
            )
            sendMessage(chat_id=msg.chat.id,
                        text="Введите запрос не менее 3 символов",
                        reply_markup=markup)
            return
        else:
            msg1 = sendMessage(chat_id=msg.chat.id,
                               text="*Ищу...*")
            if not book:
                if not subject:
                    cursor.execute("SELECT id, song FROM songs")
                    data = cursor.fetchall()
                else:
                    cursor.execute(f"SELECT id, song FROM songs WHERE subject = {subject}")
                    data = cursor.fetchall()
            else:
                cursor.execute(f"SELECT id, song FROM songs WHERE songbook_id = '{book}'")
                data = cursor.fetchall()
            result = []
            result_text = ""
            result_text1 = ""
            i = 1
            for song in data:
                res = fuzz.token_set_ratio(text, song[1])  # process.extractOne(text, song[1:])[1]
                if res >= 70:  # fuzz.WRatio(text, song[2]) >= 60:
                    result.append({"id": song[0], "res": res})
            for res in sorted(result, key=lambda r: r["res"], reverse=True):
                bot.send_chat_action(chat_id=msg.chat.id, action='typing')
                if i > 10:
                    break
                cursor.execute(f"SELECT title, songbook_id, songbook_number, id FROM songs WHERE id = {res['id']}")
                data_song = cursor.fetchone()
                cursor.execute(f"SELECT title FROM song_book WHERE id = {data_song[1]}")
                data_book = cursor.fetchone()
                # result_text = result_text + f"{i} - {data_song[0]} (`{data_book[0]} №{data_song[2]}`)\n"
                result_text1 = result_text1 + f"Название: `{data_song[0]}`\n" \
                                              f"Сборник: `{data_book[0]} №{data_song[2]}`\n" \
                                              f"Посмотреть: /song{data_song[3]}\n\n"
                """if i % 10 == 1:
                    markup.add(
                        telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                           callback_data=f'song {res["id"]}')
                    )
                elif 1 < i % 10 <= 5:
                    markup.keyboard[0].append(
                        telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                           callback_data=f'song {res["id"]}')
                    )
                elif i % 10 == 6:
                    markup.add(
                        telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                           callback_data=f'song {res["id"]}')
                    )
                else:
                    markup.keyboard[1].append(
                        telebot.types.InlineKeyboardButton(text=button_number[f"{i % 10}"],
                                                           callback_data=f'song {res["id"]}')
                    )"""
                i = i + 1
        if result_text1 == "":
            result_text1 = "Не у далось найти песню, повторите запрос"
            markup.add(telebot.types.InlineKeyboardButton(text='❔Как искать?',
                                                          callback_data='info_search'))
        else:
            markup.add(
                telebot.types.InlineKeyboardButton(text='❔Как искать?',
                                                   callback_data='info_search'),
                telebot.types.InlineKeyboardButton(text='❌Закрыть',
                                                   callback_data='delete')
            )
        markup.add(
            telebot.types.InlineKeyboardButton(text="🔎Искать в чате",
                                               switch_inline_query="")
        )
        msg2 = bot.edit_message_text(
            chat_id=msg.chat.id,
            message_id=msg1.message_id,
            text=result_text1
        )
        bot.edit_message_reply_markup(
            chat_id=msg2.chat.id,
            message_id=msg2.message_id,
            reply_markup=markup
        )
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id=msg.chat.id, text=text, args=e.args)


@bot.inline_handler(func=lambda query: True)
def query_inline(query):
    global data_songs, books
    try:
        result_query = []
        i = 1
        if query.query.isdigit():  # Search number
            if not data_songs:
                cursor.execute("SELECT title, song, songbook_id, songbook_number, tune, id FROM songs")
                data_songs = cursor.fetchall()
                cursor.execute(f"SELECT id, title FROM song_book")
                books = cursor.fetchall()
            result = []
            i = 1
            for song in data_songs:
                if song[3] in int(query.query):
                    result.append(
                        {
                            "id": song[5],
                            "title": song[0],
                            "song": song[1],
                            "book": sorted(books, key=lambda b: b[0])[int(song[2]) - 1][1],
                            "number": song[3],
                            "tune": song[4]
                        })
            for res in sorted(result, key=lambda r: r["number"]):
                if i > 10:
                    break
                markup = telebot.types.InlineKeyboardMarkup()
                markup.add(
                    telebot.types.InlineKeyboardButton(text='ℹО песне',
                                                       callback_data=f'info_song {res["id"]}'),
                    telebot.types.InlineKeyboardButton(text='Перейти в бота',
                                                       url=f'tg://resolve?domain=ChristianSongbot&start={query.query}'),
                )
                markup.add(
                    telebot.types.InlineKeyboardButton(text="🔎Искать в чате",
                                                       switch_inline_query="")
                )
                result_query.append(
                    telebot.types.InlineQueryResultArticle(id=i,
                                                           title=f"{i} {res['title']}\n",
                                                           description=f"{res['book']} №{res['number']}",
                                                           input_message_content=telebot.types.InputTextMessageContent(
                                                               message_text=song_text(
                                                                   title=res['title'],
                                                                   book=res['book'],
                                                                   number=res['number'],
                                                                   tune=res['tune'],
                                                                   song=res['song']
                                                               ),
                                                               parse_mode="MARKDOWN"
                                                           ),
                                                           reply_markup=markup))
                i += 1
            if not result_query:
                markup = telebot.types.InlineKeyboardMarkup()
                markup.add(
                    telebot.types.InlineKeyboardButton(text='Перейти в бота',
                                                       url=f'tg://resolve?domain=ChristianSongbot&start={query.query}'),
                )
                markup.add(
                    telebot.types.InlineKeyboardButton(text="🔎Искать в чате",
                                                       switch_inline_query="", )
                )
                result_query.append(telebot.types.InlineQueryResultArticle(
                    id=1,
                    title="🚫Поиск не дал результата",
                    input_message_content=telebot.types.InputTextMessageContent(
                        message_text=f"🚫По запросу `{query.query}` не нашлось ни одной песни",
                        parse_mode="MARKDOWN"
                    ),
                    reply_markup=markup
                ))
        elif len(query.query) >= 3:  # Search text
            if not data_songs:
                cursor.execute("SELECT title, song, songbook_id, songbook_number, tune, id FROM songs")
                data_songs = cursor.fetchall()
                cursor.execute(f"SELECT id, title FROM song_book")
                books = cursor.fetchall()
            result = []
            for song in data_songs:
                res = fuzz.token_set_ratio(query.query, song[1])  # process.extractOne(text, song[1:])[1]
                if res >= 70:  # fuzz.WRatio(text, song[2]) >= 60:
                    result.append(
                        {
                            "id": song[5],
                            "res": res,
                            "title": song[0],
                            "song": song[1],
                            "book": sorted(books, key=lambda b: b[0])[int(song[2]) - 1][1],
                            "number": song[3],
                            "tune": song[4]
                        })
            for res in sorted(result, key=lambda r: r["res"], reverse=True):
                if i > 10:
                    break
                markup = telebot.types.InlineKeyboardMarkup()
                markup.add(
                    telebot.types.InlineKeyboardButton(text='ℹО песне',
                                                       callback_data=f'info_song {res["id"]}'),
                    telebot.types.InlineKeyboardButton(text='Перейти в бота',
                                                       url=f'tg://resolve?domain=ChristianSongbot&start={query.query}'),
                )
                markup.add(
                    telebot.types.InlineKeyboardButton(text="🔎Искать в чате",
                                                       switch_inline_query="", )
                )
                result_query.append(
                    telebot.types.InlineQueryResultArticle(id=i,
                                                           title=f"{i} {res['title']}\n",
                                                           description=f"{res['book']} №{res['number']}",
                                                           input_message_content=telebot.types.InputTextMessageContent(
                                                               message_text=song_text(
                                                                   title=res['title'],
                                                                   book=res['book'],
                                                                   number=res['number'],
                                                                   tune=res['tune'],
                                                                   song=res['song']
                                                               ),
                                                               parse_mode="MARKDOWN"
                                                           ),
                                                           reply_markup=markup))
                i += 1
            if not result_query:
                markup = telebot.types.InlineKeyboardMarkup()
                markup.add(
                    telebot.types.InlineKeyboardButton(text=f'Перейти в бота',
                                                       url=f'tg://resolve?domain=ChristianSongbot&start={query.query}'),
                )
                markup.add(
                    telebot.types.InlineKeyboardButton(text="🔎Искать в чате",
                                                       switch_inline_query="")
                )
                result_query.append(telebot.types.InlineQueryResultArticle(
                    id=1,
                    title="🚫Поиск не дал результата",
                    description="Попробуй изменить свой зопрос.",
                    input_message_content=telebot.types.InputTextMessageContent(
                        message_text=f"🚫По запросу `{query.query}` не нашлось ни одной песни",
                        parse_mode="MARKDOWN"
                    ),
                    reply_markup=markup
                ))
        else:
            markup = telebot.types.InlineKeyboardMarkup()
            markup.add(
                telebot.types.InlineKeyboardButton(text='Перейти в бота',
                                                   url=f'tg://resolve?domain=ChristianSongbot&start={query.query}'),
            )
            markup.add(
                telebot.types.InlineKeyboardButton(text="🔎Искать в чате",
                                                   switch_inline_query="")
            )
            result_query.append(telebot.types.InlineQueryResultArticle(
                id=1,
                title="❗Введите запрос (не менее 3 символов)",
                description="Введите текст из песни, или номер песни в сборнике, я попытаюсь найти эту песню.",
                input_message_content=telebot.types.InputTextMessageContent(
                    message_text=f"Поиск можно осуществить набрав не менее 3 символов.",
                    parse_mode="MARKDOWN"
                ),
                reply_markup=markup
            ))
        bot.answer_inline_query(inline_query_id=query.id,
                                results=result_query,
                                cache_time=10,
                                switch_pm_text=f"{i - 1} рез. Перейти в бота",
                                switch_pm_parameter="start"
                                )
    except Exception as e:
        error(traceback.format_exc(limit=1010),
              chat_id=query.from_user.id, text=f"`Inline mode`\n{query.query}", args=e.args, inline=True)


bot.polling(none_stop=True, interval=0, timeout=20)
