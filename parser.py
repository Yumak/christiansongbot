import json

from bs4 import BeautifulSoup

fd = open('001_Ya_slyshu_zov_lyubvi.xml', 'r', encoding="utf-8")

xml_file = fd.read()

soup = BeautifulSoup(xml_file, 'lxml')
song = {
     "id": None,
     "title": None,
     "tune": None,
     "text": None,
     "subject": {
          "id": None,
          "name": None
     }
}
i = 0
for tag in soup.findAll("credit-words"):
     if i == 0:
          song["id"] = tag.text[0:-2]
     elif i == 1:
          song["title"] = tag.text
     elif i == 2:
          song["text"] = tag.text.replace('\n\n', '\n')
     else:
          song["text"] = song["text"] + tag.text[1:-1]
     i = i + 1

for tag1 in soup.findAll("sign"):
     for tag2 in soup.findAll("mode"):
          song["tune"] = f"{tag1.text}-{tag2.text}"

print(json.dumps(song, indent=2, ensure_ascii=False))
fd.close()
