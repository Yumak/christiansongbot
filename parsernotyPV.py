import requests
import psycopg2
import json

conn = psycopg2.connect(dbname='christiansongsbot',
                        user='christiansongsbot',
                        password='2001',
                        host='46.17.104.151')
conn.autocommit = True
cursor = conn.cursor()
try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

url = "https://noty.propovednik.com/?dir=_PV/%D0%9F%D0%B5%D1%81%D0%BD%D0%B8%20%D0%A5%D1%80%D0%B8%D1%81%D1%82%D0%B8%D0%B0%D0%BD%202002/%D0%9F%D0%BE%20%D0%BD%D0%BE%D0%BC%D0%B5%D1%80%D0%B0%D0%BC%20%D0%BF%D0%B5%D1%81%D0%B5%D0%BD%20%28%D0%BD%D0%BE%D1%82%D1%8B%29"
PV800_json = {}

with open("./files/PV800.json", "r", encoding="utf-8") as file:
    PV800_json = json.load(file)

request = requests.get(url)
html = request.text
parsed_html = BeautifulSoup(html, "lxml")
al = parsed_html.body.find_all('a', attrs={'class': 'item file'})

for song in PV800_json["songs"]:
    for a in al:
        if int(a.text[0:3]) == song["id"]:
            try:
                song["noty"].append(
                    {
                        "name": a.text,
                        "href": f"https://noty.propovednik.com/{a.get('href')}"
                    })
            except KeyError as e:
                song["noty"] = []
                song["noty"].append(
                    {
                        "name": a.text,
                        "href": f"https://noty.propovednik.com/{a.get('href')}"
                    })


for song in PV800_json["songs"]:
    cursor.execute(f"UPDATE songs SET noty = '{json.dumps({'noty': song['noty']}, ensure_ascii=False)}' WHERE id = {song['id']}")

print("ok")


with open("./files/PV800_noty.json", "w", encoding="utf-8") as file:
   file.write(json.dumps(PV800_json, indent=4, ensure_ascii=False))
