import time

import telebot
import psycopg2
import traceback
import requests

TOKEN = "1648983636:AAEXqhvNiXDZA_ZrQfWtsJn3dXrpglhLBzo"
ADMIN_CHAT_ID = 1093110311
ERROR_CHAT_ID = -574533907
PASS = "ок"


bot = telebot.TeleBot(token=TOKEN, parse_mode="MARKDOWN")
conn = psycopg2.connect(dbname='christiansongsbot',
                        user='christiansongsbot',
                        password='2001',
                        host='46.17.104.151')
conn.autocommit = True
cursor = conn.cursor()

print("listen bot...")


def error(e: str, chat_id: int, text: str, args: str):
    requests.get(f"https://api.telegram.org/bot{TOKEN}/sendmessage",
                 {
                     "chat_id": ERROR_CHAT_ID,
                     "text": f"chat-id: `{chat_id}`\n"
                             f"text: `{text}`\n"
                             f"time: `{str(time.ctime())}`\n"
                             f"args: `{args}`",
                     "parse_mode": "MARKDOWN"
                 })
    requests.get(f"https://api.telegram.org/bot{TOKEN}/sendmessage",
                 {
                     "chat_id": ERROR_CHAT_ID,
                     "text": f"`{e}`",
                     "parse_mode": "MARKDOWN"
                 })
    requests.get(f"https://api.telegram.org/bot{TOKEN}/sendmessage",
                 {
                     "chat_id": chat_id,
                     "text": f"Приносим свои извенения, на сервере бота произошла ошибка, "
                             f"пожалуйста перезагрузите бота командой /start и повторите запрос.",
                     "parse_mode": "MARKDOWN"
                 })


def sendMessage(chat_id: int, text: str, reply_markup: telebot.types.ReplyKeyboardMarkup = None,
                reply_to_message_id: int = None):
    try:
        bot.send_chat_action(chat_id=chat_id, action='typing')
        bot.send_message(chat_id=chat_id,
                         text=text,
                         reply_markup=reply_markup,
                         reply_to_message_id=reply_to_message_id)
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id, text, e.args)


@bot.message_handler(commands=['start'])
def start(msg):
    try:
        bot.send_chat_action(chat_id=msg.chat.id, action='typing')
        if msg.chat.type != 'private':
            return
        else:
            cursor.execute(f'SELECT * FROM "status-add" WHERE chat_id = {msg.chat.id};')
            data = cursor.fetchone()
            if not data:
                cursor.execute(
                    f'INSERT INTO "status-add" (chat_id, status) VALUES (%s, %s);',
                    (msg.chat.id, "start"))
            else:
                cursor.execute(f'UPDATE "status-add" SET status = {"start"} WHERE chat_id = {msg.chat.id};')
            sendMessage(chat_id=msg.chat.id,
                        text="/new - Добавление псни в базу данных\n"
                             "/edit - Изменение песни в базе данных\n"
                             "Что нужно чтобы добавить песню в БД:\n"
                             "Название песни, Текст песни, Тег(к какому празднику или событию), "
                             "Сборник из которого песня, Ссылка на pdf файл нот на сайте https://noty-bratstvo.org/, "
                             "Тональность песни.\n"
                             "*При добавлении песни, убедитесь что этой песни нет в базе бота*")
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id=msg.chat.id, text=msg.text, args=e.args)


@bot.message_handler(commands=['new'])
def new(msg):
    try:
        bot.send_chat_action(chat_id=msg.chat.id, action='typing')
        if msg.chat.type != 'private':
            return
        else:
            cursor.execute(f'UPDATE "status-add" SET status = {"new"} WHERE chat_id = {msg.chat.id};')
            bot.send_chat_action(chat_id=msg.chat.id, action='typing')
            sendMessage(chat_id=msg.chat.id,
                        text="Пришли мне название песни либо первую строчку этой песни.")
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id=msg.chat.id, text=msg.text, args=e.args)


@bot.message_handler(commands=['edit'])
def edit(msg):
    try:
        bot.send_chat_action(chat_id=msg.chat.id, action='typing')
        if msg.chat.type != 'private':
            return
        else:
            cursor.execute(f'UPDATE "status-add" SET status = {"new"} WHERE chat_id = {msg.chat.id};')
            bot.send_chat_action(chat_id=msg.chat.id, action='typing')
            sendMessage(chat_id=msg.chat.id,
                        text="Пришли мне название песни либо первую строчку этой песни.")
    except Exception as e:
        error(traceback.format_exc(limit=1010), chat_id=msg.chat.id, text=msg.text, args=e.args)


