import time

import psycopg2
import json

conn = psycopg2.connect(dbname='christiansongsbot',
                        user='christiansongsbot',
                        password='2001',
                        host='46.17.104.151')
conn.autocommit = True
cursor = conn.cursor()

DH = {}
PV = {}
TP = {}

with open("./files/DH.json", "r", encoding="utf-8") as file:
    DH = json.load(file)

with open("./files/PV800.json", "r", encoding="utf-8") as file:
    PV = json.load(file)

with open("./files/TP.json", "r", encoding="utf-8") as file:
    TP = json.load(file)


def dh():
    for song in DH["songs"]:
        cursor.execute(
            f'INSERT INTO "songs" (id,'
            f'title, '
            f'song, '
            f'tags, '
            f'songbook_id, '
            f'songbook_number, '
            f'created_at, '
            f'updated_at, '
            f'tune, '
            f'subject) '
            f'VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);',
            (
                800 + song["id"],
                song["title"],
                song["text"],
                "",
                2,
                song["id"],
                time.time(),
                0,
                song["tune"],
                song["subject"]["id"]
            ))
    print("ok")
    for song in TP["songs"]:
        cursor.execute(
            f'INSERT INTO "songs" (id,'
            f'title, '
            f'song, '
            f'noty, '
            f'songbook_id, '
            f'songbook_number, '
            f'created_at, '
            f'updated_at, '
            f'tune, '
            f'subject) '
            f'VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);',
            (
                800 + 359 + song["id"],
                song["title"],
                song["text"],
                "",
                3,
                song["id"],
                time.time(),
                0,
                song["tune"],
                song["subject"]["id"]
            ))
    print("ok")
    return


if __name__ == '__main__':
    dh()
